package com.example.phonebook.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
// import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "user_contact")
public class Contact {

    @Id
    @GeneratedValue
    private Long id;
    @NonNull
    private String name;
    private String phoneNumber;
    @OneToOne(cascade=CascadeType.PERSIST)
    private User user;

    public Long getId() {
        return id;
    }

    // @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    // private Set<Event> events;
}